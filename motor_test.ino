//char speeds[5] = {'!', '?', 'T', 'd', '}'};
byte speeds[5] = {50, 100, 127, 160, 200}; //to make sure I don't break the motors 
//int speeds[5] = {0, 50, 100, 200, 255};
int a = 0;
int b = 0;
int maximum = 5;
unsigned long now;

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
}

void loop() {
  if (millis() > now + 2000) { //change every 2secs
    now = millis(); //update when the last check was done

    //change speeds
    a = alter(a, false);
    b = alter(b, false);
    
    //output and control
    Serial.println(now);
    Serial.print(speeds[a]);
    Serial.print(", ");
    Serial.println(speeds[b]);
    Serial1.write(speeds[a]);
    Serial2.write(speeds[b]);
  }

  if (millis() > 10000) {
    Serial1.write(128);
    Serial2.write(128);
    Serial.println("Done");
    while(1);
  }
}

int alter(int v, bool b) {
  if (b) {
    v--;
  } else {
    v++;
  }
  if (v > 4) {v = 0;}
  if (v < 0) {v = 4;}
  return v;
}
